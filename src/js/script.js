const primeiroDepoimento = document.getElementById('paragrafo');
const nomeAlvaro = 'Álvaro Rodrigues';

primeiroDepoimento.innerText = `“Nós estamos com um elenco de cinco pessoas em cena. Quando a gente foi trabalhar esse recorte, fizemos uma reflexão sobre os próprios personagens. 
Mais do que contar histórias de academia, vamos contar histórias que são vivenciadas lá dentro. 
Satirizar o trivial. Não consigo me lembrar de uma peça que se passa dentro de uma academia em BH”, conta o diretor ${nomeAlvaro}.`

/**************************************************************************************************************************************/

const segundoDepoimento = document.getElementById('demo');
const nomeAline = 'Aline Alves Ribeiro';

segundoDepoimento.innerText = `“Vão rir, rir e rir. Vão tentar descobrir quem está fazendo coisas no vestiário. O final é surpreendente”, antecipou ${nomeAline}.`

/**************************************************************************************************************************************/

const terceiroDepoimento = document.getElementById('info');
const nomeJefferson = ' Jefferson Beethoven';

terceiroDepoimento.innerText = `“Ele se convence de que consegue fazer isso com fórmulas mágicas, colocando suplementos em alimentos enriquecidos corpo adentro. Faz 100 kg de supino – um tipo de aparelho – e, no dia seguinte, vai ficar fortão”, conta ${nomeJefferson}.`

/**************************************************************************************************************************************/

const titulo = document.querySelector('#titulo_noticia');

titulo.innerText = 'Comédia inédita promete muitas risadas em BH';